import React, {useState} from 'react';
import {Route, Redirect} from 'react-router-dom';
import {withTracker} from "meteor/react-meteor-data";
const ${NAME} = ({component: Component, ...rest}) => {
    const [modalState, setModal] = useState(null);
    UTILS.modal.setSetModal(setModal);
    const redirectRoute = UTILS.ui.limitedAccessController(rest);
	return (
		<Route {...rest} render={matchProps => (
			<div className="layout ${NAME}">
				<div className="content">
					<Component {...matchProps} />
				</div>
				{ modalState &&
                    <div className={'modalWrap'}>
                        <div className={'modalContent'}>
                            <div className={"modalUnderlay"} onClick={UTILS.ui.closeModal}/>
                            <modalState.Component {...modalState.data}/>
                        </div>
                    </div>
                }
			</div>
		)} />
	)
};
export default ${NAME}Container = withTracker(() => {
	return {};
})(${NAME});