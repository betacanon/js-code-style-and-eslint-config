import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
class ${NAME} extends Component {
	constructor() {
		super();
		this.state = {
			isSubmitting: false
		}
	}
	componentDidMount() {}
	componentWillUnmount() {}
	render() {
		return (
			<div className="${NAME}">
			</div>
		);
	}
}
const ${NAME}Container = withTracker(() => {
    return {};
})(${NAME});
export default ${NAME}Container;