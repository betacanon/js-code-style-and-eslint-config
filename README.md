#Requires node.js##
[Download](https://nodejs.org/en/download/)

`PATH usr/local/bin/node`
---
##Terminal commands for each project:
`meteor npm install eslint --save-dev`

`meteor npm install eslint-plugin-meteor --save-dev`
---
##Files:
###Add both files from this repo to the root of project folder
- .eslintrc.json
- .eslintignore
---
##Webstorm Config:
1. Webstorm > Preferences > Languages & Frameworks > Javascript > Code Quality Tools > ESLint
2. Enable (Top Left)
3. Node Interpeter should be set to PATH where you downloaded Node.js
4. ESLint package resides in ../node_modules/eslint
---

#When and Where to Use:
##Setup
1. Webstorm > Preferences > Keymap > Plug-Ins > Javascript Support > Fix ESLint Problems
2. Double Click > Add keyboard shortcut
3. `CMD + Shift + L` is available and stands for 'Lint'
4. Use on every file to auto apply linting rules

**Use `eslint --fix .` in Terminal to apply rule changes to entire project**

*Add any file you want to ignore Linting rule changes to the `.eslintignore` file*



*Please make changes to Linting rules in a feature/branch and create Pull Request to have them added to company policy.*